# Front-End Dev
 
**Estamos contratando engenheiro de software front-end.** 

## Sobre nós:
Somos uma de startup de tecnologia educacional. Estamos comprometidos em criar um mundo de autodidatas, apaixonados por aprendizagem e inteligência artificial. Criamos inteligência para que pessoas desenvolvam a sua. 

Temos a filosofia de manter a tecnologia do jeito mais simples e elegante possível, todo o nosso stack de desenvolvimento reflete isso. Buscamos qualidade de código e da arquitetura, sempre deixando nossos testes em dia.  Acreditamos que o desejo contínuo de aprender é a característica mais importante em qualquer profissional, estamos sempre descobrindo novas tecnologias que resolvam os problemas de um jeito melhor. 

## Nosso Stack:
Mongo - Flask - Eve - Quasar

**Tecnologias que usamos:**
	- Js, Python, Mongo, Docker, Linux, Machine Learning

## Estamos procurando alguém que:

 - Esteja em constante processo de aprendizagem (Busque conhecimento)
 - Tenha sólidos conhecimentos em computação
 - Saiba trabalhar em equipe, aprendendo e ensinando
 - Tenha experiência em Desenvolvimento Web
 - Saiba usar Git e ambiente Unix

## Experiências que  são um extra:

 - JavaScript ES6
 - SPA ( Vue, React, Angular)
 - Python (Flask, Eve)
 - MongoDB
 - Docker (Rancher, Kubernetes)

## Atrativos
 - Ambiente de trabalho informal
 - Organização flexível, todos são convidados a contribuir
 - Escritório abastecido com guloseimas
 - Liberdade de escolha em seu ambiente de desenvolvimento
 - Salário competitivo

## Como participar
Processo seletivo: (https://gitlab.com/tieduqc/jobs)

Contato: ti@eduqc.com



# Proposta

Apresentar uma aplicação SPA [Vue, React, Angular, Riot]  que consuma uma API local disponibilizada. A aplicação terá:

- Uma tela de filtro do conteúdo armazenado no banco



### Requisitos

#### Guia de interface

Há total liberdade quanto ao design a ser adotado para apresentar o conteúdo do banco, porém alguns pontos devem ser obedecidos:

- As cores da empresa devem ser utilizadas
```
	$primary   = #a6ce38
	$secondary = #016db9
```
- Para ter acesso ao conteúdo do banco, deve ser necessário fazer login.
	- O acesso à API requer autentificação: `user:senhaforte`

#### Pontos mínimos necessários

- Documentação (de preferência em inglês)
- Uso da [Integração Continua](https://about.gitlab.com/features/gitlab-ci-cd/) do Gitlab para a criação de um contêiner válido no 
*registry* do repositório.

#### Bônus

- Um tipo de teste automatizado para validar o CI.
- Login de usuário

### Termos de entrega

A entrega da proposta pode ser feita a partir de um *Merge Request* neste repositório ou por meio de um *patch* gerado a partir do *diff* para o email [ti@eduqc.com](mailto:ti@eduqc.com).
Também inclua seu CV com informação de contato como um arquivo do projeto


### Fazendo de uso da API

A API oferecida neste repositório faz de uso do *framework* [Eve](http://python-eve.org/) e de um contêiner [docker](https://www.docker.com/) com um banco [mongo](https://docs.mongodb.com/).

Com o contêiner do banco rodando, a API pode ser levantada com:

	$ python api.py

A API estará disponível na porta `8000`.

### Mostrando seu processo de Raciocínio
Não hesite em nos contactar com qualquer dúvida que apareça no meio do caminho.
Durante a execução do projeto, é mais importante documentar seu raciocínio do que entregar um projeto super completo. 
Boa sorte a todos! 

A data limite para submissões é dia:
# 20/05/2018
